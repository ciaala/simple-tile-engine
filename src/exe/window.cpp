/*
 * window.cpp
 *
 *  Created on: 25/ago/2014
 *      Author: tudhalyas
 */

#include <SFML/Graphics.hpp>

sf::Uint32 style;
bool vsync;
sf::RenderWindow window;

bool isFullscreen()
{
	return (style & sf::Style::Fullscreen) > 0;
}

void toggleFullscreen()
{
	if (isFullscreen())
		style = sf::Style::Default;
	else
		style = sf::Style::Default | sf::Style::Fullscreen;
}

void toggleVSync()
{
	vsync = !vsync;
	window.setVerticalSyncEnabled(vsync);
}

void createWindow(int width, int height)
{
	window.create(sf::VideoMode(width, height), "My window", style);
	window.setVerticalSyncEnabled(vsync);

	if (!isFullscreen())
	{
		sf::VideoMode desktop = sf::VideoMode::getDesktopMode();
		sf::Vector2i pos(desktop.width / 2 - window.getSize().x / 2,
				desktop.height / 2 - window.getSize().y / 2);
		window.setPosition(pos);
	}
}

int main()
{
	style = sf::Style::Default;
	vsync = true;
	createWindow(800, 600);

	// run the program as long as the window is open
	while (window.isOpen())
	{
		// check all the window's events that were triggered since the last iteration of the loop
		sf::Event event;
		while (window.pollEvent(event))
		{
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num1))
			{
				// change resolution to 800x600
				createWindow(800, 600);
			}
			else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num2))
			{
				// change resolution to 1920x1080
				createWindow(1920, 1080);
			}
			else if (sf::Keyboard::isKeyPressed(sf::Keyboard::F))
			{
				// toggle fullscreen
				toggleFullscreen();
				createWindow(window.getSize().x, window.getSize().y);
			}
			else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
				window.close();

			// "close requested" event: we close the window
			if (event.type == sf::Event::Closed)
				window.close();
		}

		window.clear();
		window.display();
	}

	return 0;
}
