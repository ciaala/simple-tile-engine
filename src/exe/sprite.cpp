/*
 * sprite.cpp
 *
 *  Created on: 26/ago/2014
 *      Author: tudhalyas
 */

#include <SFML/Graphics.hpp>
#include <iostream>

sf::Uint32 style;
bool vsync;
sf::RenderWindow window;
sf::Image iElf, iCentaur;
sf::Texture tElf, tCentaur;
sf::Sprite sElf, sCentaur;
sf::Color alphaKey(32, 156, 0);

void loadMedia()
{
/*
	sf::IntRect subRect;
	subRect.top = 0;
	subRect.left = 0;
	subRect.height = 32;
	subRect.width = 24;
*/

	if (!iElf.loadFromFile("resources/textures/elf_warrior.png"))
	{
		std::cerr << "Could not load 'elf_warrior.png'!" << std::endl;
		exit(-1);
	}

	iElf.createMaskFromColor(alphaKey);
	tElf.loadFromImage(iElf);
	std::cout << "Texture 'elf_warrior.png' loaded" << std::endl;

	if (!iCentaur.loadFromFile("resources/textures/female_centaur.png"))
	{
		std::cerr << "Could not load 'female_centaur.png'!" << std::endl;
		exit(-1);
	}

	iCentaur.createMaskFromColor(alphaKey);
	tCentaur.loadFromImage(iCentaur);
	std::cout << "Texture 'female_centaur.png' loaded" << std::endl;
}

void setupSprites()
{
	sElf.setTexture(tElf, true);
	sElf.setScale(sf::Vector2f(3, 3));
	sElf.setPosition(100, 100);

	sCentaur.setTexture(tCentaur, true);
	sCentaur.setScale(sf::Vector2f(3, 3));
	sCentaur.setPosition(400, 100);
}

void createWindow(int width, int height)
{
	window.create(sf::VideoMode(width, height), "My window", style);
	window.setVerticalSyncEnabled(vsync);

	sf::VideoMode desktop = sf::VideoMode::getDesktopMode();
	sf::Vector2i pos(desktop.width / 2 - window.getSize().x / 2,
			desktop.height / 2 - window.getSize().y / 2);
	window.setPosition(pos);
}

int main()
{
	style = sf::Style::Default;
	vsync = true;

	loadMedia();
	setupSprites();

	createWindow(800, 600);

	// run the program as long as the window is open
	while (window.isOpen())
	{
		// check all the window's events that were triggered since the last iteration of the loop
		sf::Event event;
		while (window.pollEvent(event))
		{
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
				window.close();

			// "close requested" event: we close the window
			if (event.type == sf::Event::Closed)
				window.close();
		}

		window.clear();
		window.draw(sCentaur);
		window.draw(sElf);
		window.display();
	}

	return 0;
}
