/*
 * Tileset.cpp
 *
 *  Created on: 26/ago/2014
 *      Author: tudhalyas
 */

#include "include/Tileset.h"

#include <ogdt/common/include/Exception.h>
#include <sstream>

namespace ogdt
{

Tileset::Tileset(std::string &filename, int tileWidth, int tileHeight) :
		tileWidth(tileWidth), tileHeight(tileHeight)
{
	if(!this->loadFromFile(filename))
	{
		std::stringstream ss;
		ss << "ogdt::Tileset : Cannot load '" << filename << "'!";
		throw Exception(ss.str());
	}
}

Tileset::~Tileset()
{
}

} /* namespace ogdt */
