/*
 * Tileset.h
 *
 *  Created on: 26/ago/2014
 *      Author: tudhalyas
 */

#ifndef TILESET_H_
#define TILESET_H_

#include <SFML/Graphics/Rect.hpp>
#include <SFML/Graphics/Image.hpp>
#include <SFML/System/Vector2.hpp>

namespace ogdt
{

class Tileset: public sf::Image
{
public:
	Tileset(std::string &filename, int tileWidth, int tileHeight);
	virtual ~Tileset();

	inline sf::IntRect getTileRect(int x, int y)
	{
		sf::IntRect rect;
		rect.left = x * tileWidth;
		rect.top = y * tileHeight;
		rect.width = tileWidth;
		rect.height = tileHeight;

		//if(rect.left < 0 || rect.left + rect.width > this->getSize().x)
		// ..
		//if(rect.top < 0 || rect.top + rect.height > this->getSize().y)
		// ..

		return rect;
	}

private:
	int tileWidth;
	int tileHeight;
};

} /* namespace ogdt */

#endif /* TILESET_H_ */
