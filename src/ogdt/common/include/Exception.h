/*
 * Exception.h
 *
 *  Created on: 26/ago/2014
 *      Author: tudhalyas
 */

#ifndef EXCEPTION_H_
#define EXCEPTION_H_

#include <stdexcept>
#include <SFML/System/String.hpp>

namespace ogdt
{

class Exception : public std::exception
{
public:
	Exception(const sf::String& msg);
	virtual ~Exception() throw();
	virtual const char* what() const throw();
private:
	sf::String msg;
};

} /* namespace ogdt */

#endif /* EXCEPTION_H_ */
