/*
 * Exception.cpp
 *
 *  Created on: 26/ago/2014
 *      Author: tudhalyas
 */

#include "include/Exception.h"

namespace ogdt
{

Exception::Exception(const sf::String& msg): msg(msg)
{
}

Exception::~Exception() throw()
{
}

const char* Exception::what() const throw()
{
	return msg.toAnsiString().c_str();
}

} /* namespace ogdt */
